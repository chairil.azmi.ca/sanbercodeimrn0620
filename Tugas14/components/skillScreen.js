import React, { Component } from 'react'
import { View, 
  StyleSheet, 
  Text,
  Image,
  TouchableOpacity,
  FlatList 
} from 'react-native'
import {MaterialCommunityIcons} from '@expo/vector-icons'
import data from '../skillData.json'

class Item extends React.Component {
  render(){
    let data = this.props.data
    return(
      <View style={{flex: 1, flexDirection: 'column', marginBottom: 15}}>
        <TouchableOpacity style={styles.boxSkillItem}>
          <View>
            <MaterialCommunityIcons name={data.iconName} style={{fontSize: 80, color: '#003366'}}/>
          </View>
          <View style={{justifyContent: 'center', marginHorizontal: 10, width: 170}}>
            <Text style={styles.boxSkillItemName}>{data.skillName}</Text>
            <Text style={styles.boxSkillItemCategory}>{data.categoryName}</Text>
            <View style={{alignItems: 'flex-end'}}>
              <Text style={styles.boxSkillItemPercentage}>{data.percentageProgress}</Text>
            </View>
          </View>
          <View style={{justifyContent: 'flex-end'}}>
            <MaterialCommunityIcons name="chevron-right" style={{fontSize: 80, color: '#003366'}}/>
          </View>
        </TouchableOpacity>
      </View>
    )
  }
};

export default class Note extends React.Component {
  render(){
    return(
      <View style={styles.container}>
          <View style={styles.header}>
            <Image source={require('../assets/logo.png')} style={{width: 187.5, height: 51}}/>
          </View>
          <View style={{flexDirection: 'row', marginBottom: 16}}>
            <View style={{alignItems: 'center', justifyContent: 'center'}}>
              <MaterialCommunityIcons name="account-circle" style={{fontSize: 30, color: '#3EC6FF'}}/>
            </View>
            <View style={styles.profile}>
              <View style={{flexDirection: 'column'}}>
                <Text style={{fontSize: 12}}>Hai, </Text>
                <Text style={{fontSize: 16}}>Chairil Azmi</Text>
              </View>
            </View>
          </View>
          <View>
            <Text style={styles.title}>SKILL</Text>
          </View>
          <View style={{borderColor: '#3EC6FF', borderWidth: 2}} />
          <View style={{flexDirection: "row", alignItems: "center", justifyContent: 'center', marginVertical: 10}}>
            <TouchableOpacity style={styles.boxSkill}>
              <Text style={styles.boxSkillText}>Library / Framework</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.boxSkill}>
              <Text style={styles.boxSkillText}>Bahasa Pemrograman</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.boxSkill}>
              <Text style={styles.boxSkillText}>Teknologi</Text>
            </TouchableOpacity>
          </View>
          <FlatList
            data={data.items}
            renderItem={(data) => <Item data={data.item}/>}
            keyExtractor={(item) => item.id}
            
          ></FlatList>
      </View>      
    )
  }
};


const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    margin: 10
  },
  header: {
    alignItems: "flex-end"
  },
  profile: {
    flexDirection: 'row',
    alignItems: 'center', justifyContent: 'center',
    marginLeft: 10
  },
  title: {
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'normal',
     fontSize: 36,
     lineHeight: 42,
     color: '#003366'
  },
  boxSkill: {
    padding: 10,
    backgroundColor: '#B4E9FF',
    height: 32,
    borderRadius: 8,
    alignItems:'center',
    justifyContent: 'center',
    paddingHorizontal: 8,
    paddingVertical: 9,
    marginHorizontal: 3
  },
  boxSkillText: {
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 12,
    lineHeight: 14
  },
  boxSkillItem: {
    flex: 1,
    flexDirection: "row",
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: 5,
    height: 129,
    backgroundColor: '#B4E9FF',
    borderRadius: 8,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,  
    elevation: 5 
  },
  boxSkillItemName:{
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 24,
    lineHeight: 28,
    color: '#003366',
    alignSelf: 'flex-start'
  },
  boxSkillItemCategory: {
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 16,
    lineHeight: 19,
    color: '#3EC6FF'
  },
  boxSkillItemPercentage: {
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 48,
    lineHeight: 56,
    color: '#FFFFFF'
  }
})