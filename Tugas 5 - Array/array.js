//No. 1
console.log('Soal No. 1');
function range(startNum, finishNum){;
  if(startNum && finishNum) {
    var arr = []
    if(startNum <= finishNum){
      for(var i = startNum; i <= finishNum; i++){
        arr.push(i);
      }
    } else {
      for(var i = startNum; i >= finishNum; i--){
        arr.push(i);
      }
    }
    return arr;
  } else {
    return -1
  }
}

console.log(range(10, 1));


//No. 2
console.log('\n\nSoal No. 2')
function rangeWithStep(startNum, finishNum, step){;
  if(startNum && finishNum) {
    var arr = []
    if(startNum <= finishNum){
      for(var i = startNum; i <= finishNum; i += step){
        arr.push(i);
      }
    } else {
      for(var i = startNum; i >= finishNum; i -= step){
        arr.push(i);
      }
    }
    return arr;
  } else {
    return -1
  }
}
console.log(rangeWithStep(29, 2, 4));

//No. 3
console.log('\n\nSoal No. 3');
function sum(startNum, finishNum, step){
  var result = 0;
  var s;
  if(startNum && finishNum) {
    if(step){
      s = step;
    } else {
      s = 1;
    }

    if(startNum <= finishNum){
      for(var i = startNum; i <= finishNum; i += s){
        result += i;
      }
    } else {
      for(var i = startNum; i >= finishNum; i -= s){
        result += i;
      }
    }
    return result;
  } else {
    return 0
  }
}
console.log(sum(5, 50, 2));

//No. 4
console.log('\n\nSoal No. 4');
function dataHandling(arr){
  if(Array.isArray(arr)){
    for(var i = 0; i < arr.length; i++){
      if(Array.isArray(arr[i])){
        console.log("Nomor ID: " + arr[i][0]);
        console.log("Nama Lengkap: " + arr[i][1]);
        console.log("TTL: " + arr[i][2]);
        console.log("Hobi: " + arr[i][3]);
        console.log("\n");
      }
    }
  }
}
var input = [
  ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
  ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
  ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
  ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 
dataHandling(input);

//No. 5
console.log("\n\nSoal No. 5")
function balikKata(param){
  var arr = [];
  var result = "";
  if(Array.isArray(param) || typeof param === 'string'){
    for(var i = 0; i < param.length; i++){
      arr.unshift(param[i]);
    }
  }
  for(var i = 0; i < arr.length; i++){
    result += arr[i];
  }
  return result;
}
console.log(balikKata("SanbersCode"));

//No. 6
console.log("\n\nSoal No. 6");
function dataHandling2(arr){
  if(Array.isArray(arr)){
    arr[1] = arr[1] + " Elsharawy"
    arr[2] = "Provinsi " + arr[2];
    arr.splice(4, 0, "Pria");
    arr.splice(5, 1, "SMA Internasional Metro");
    console.log(arr);

    var time = arr[3].split('/');
    var month;
    switch(parseInt(time[1])){
      case 1:
        month = "Januari"
        break;
      case 2:
        month = "Februari"
        break;
      case 3:
        month = "Maret"
        break;
      case 4:
        month = "April"
        break;
      case 5:
        month = "Mei"
        break;
      case 6:
        month = "Juni"
        break;
      case 7:
        month = "Juli"
        break;
      case 8:
        month = "Agustus"
        break;
      case 9:
        month = "September"
        break;
      case 10:
        month = "Oktober"
        break;
      case 11:
        month = "November"
        break;
      case 12:
        month = "Desember"
        break;
    }
    console.log(month);
    var timeDesc = [...time];
    // timeDesc.sort(function(a, b){return b - a});
    console.log(timeDesc.sort(function(a, b){return b - a}));
    console.log(time.join('-'));
    console.log(arr[1].slice(0, 14));
  }
}

var input2 = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input2);