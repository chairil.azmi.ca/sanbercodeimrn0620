import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, 
  Text, 
  View, 
  Image, 
  TextInput ,
  TouchableOpacity
} from 'react-native';

import {Ionicons} from '@expo/vector-icons'

export default class App extends Component {
  render(){
    return (
      <View style={styles.container}>
        <View>
          <Text>Tentang Saya</Text>
        </View>
        <View>
        <Ionicons name="ios-contact" size={25} />
        </View>
        <View>
          <Text>Chairil Azmi</Text>
        </View>
        <View>
          <Text>React Native Developer</Text>
        </View>
        <View>
          <View>
            <Text>Portofolio</Text>
          </View>
          <View>
            <View>
              <Text>@chairil.azmi.ca</Text>
            </View>
            <View>
              <Text>@azmi23</Text>
            </View>
          </View>
        </View>
        <View>
          <View>
            <Text>Hubungi Saya</Text>
          </View>
          <View>
            <View>
              <Text>@chairil_azmi_23</Text>
            </View>
          </View>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center"
  }
})