import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, 
  Text, 
  View, 
  Image, 
  TextInput ,
  TouchableOpacity
} from 'react-native';

export default class App extends Component {
  render(){
    return (
      <View style={styles.container}>
        <View style={styles.logo}>
          <Image source={require('../images/logo.png')} style={{width: 300, height: 90}}/>
        </View>
        <View style={styles.titlePage}>
          <Text style={{fontSize: 20, color: "#003366"}}>Login</Text>
        </View>
        <View style={{marginVertical: 15}}>
          <Text style={{color: "#003366"}}>Username/Email</Text>
          <TextInput style={styles.textInput}></TextInput>
        </View>
        <View style={{marginVertical: 15}}>
          <Text style={{color: "#003366"}}>Password</Text>
          <TextInput style={styles.textInput}></TextInput>
        </View>
        <View style={{alignItems: "center", marginTop: 20}}>
          <TouchableOpacity style={styles.loginBtn}>
            <Text style={{color: "white", fontSize: 18}}>Masuk</Text>
          </TouchableOpacity>
        </View>
        <View style={{alignItems: "center", marginVertical: 10}}>
          <Text style={{color: "#3EC6FF", fontSize: 15}}>atau</Text>
        </View>
        <View style={{alignItems: "center"}}>
          <TouchableOpacity style={styles.registerBtn}>
            <Text style={{color: "white", fontSize: 18}}>Daftar ?</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    padding: 20
  },
  logo: {
    justifyContent: "center",
    alignItems: "center",
    marginVertical: 40
  },
  titlePage: {
    alignItems: "center",
    justifyContent: "center"
  },
  textInput: {
    height: 40,
    borderColor: "#003366",
    borderWidth: 1,
  },
  loginBtn: {
    height: 40,
    width: 120,
    borderRadius: 10,
    backgroundColor: "#3EC6FF",
    alignItems: "center",
    justifyContent: "center"
  },
  registerBtn: {
    height: 40,
    width: 120,
    borderRadius: 10,
    backgroundColor: "#003366",
    alignItems: "center",
    justifyContent: "center"
  }
})