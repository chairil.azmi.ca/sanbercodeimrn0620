/**
 * Berikut soal quiz kali ini, terdiri dari 3 Soal
 * Kerjakan dengan sebaik mungkin, dengan menggunakan metode yang telah dipelajari,
 * Tidak diperkenankan untuk menjawab hanya dengan console.log('teks jawaban');
 * maupun dengan terlebih dahulu memasukkannya ke dalam variabel, misal var a = 'teks jawaban'; console.log(a);
 * 
 * Terdapat tambahan poin pada setiap soal yang dikerjakan menggunakan sintaks ES6 (+5 poin)
 * Jika total nilai Anda melebihi 100 (nilai pilihan ganda + coding), tetap akan memiliki nilai akhir yaitu 100
 * 
 * Selamat mengerjakan
*/

/*========================================== 
  1. SOAL CLASS SCORE (10 poin + 5 Poin ES6)
  ==========================================
  Buatlah sebuah class dengan nama Score. class Score tersebut memiliki properti "subject", "points", dan "email". 
  "points" dapat di input berupa number (1 nilai) atau array of number (banyak nilai).
  tambahkan method average untuk menghitung rata-rata dari parameter points ketika yang di input berupa array (lebih dari 1 nilai)
*/

class Score {
  constructor(subject, points, email){
    this._subject = subject
    this._email = email
    this._points = points
  }

  get getSubject(){
    return this._subject
  }

  get getEmail(){
    return this._email
  }

  get getPoints(){
    return this._points
  }

  average(){
    if(Array.isArray(this._points)){
      let result = 0
      for(let i = 0; i < this._points.length; i++){
        result = result + this._points[i]
      }
      result = result / this._points.length
      return result
    } else {
      return this._points
    }
  }
}

/*=========================================== 
  2. SOAL Create Score (10 Poin + 5 Poin ES6)
  ===========================================
  Membuat function viewScores yang menerima parameter data berupa array multidimensi dan subject berupa string
  Function viewScores mengolah data email dan nilai skor pada parameter array 
  lalu mengembalikan data array yang berisi object yang dibuat dari class Score.
  Contoh: 

  Input
   
  data : 
  [
    ["email", "quiz-1", "quiz-2", "quiz-3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88]
  ]
  subject: "quiz-1"

  Output 
  [
    {email: "abduh@mail.com", subject: "quiz-1", points: 78},
    {email: "khairun@mail.com", subject: "quiz-1", points: 95},
  ]
*/

const data = [
  ["email", "quiz-1", "quiz-2", "quiz-3"],
  ["abduh@mail.com", 78, 89, 90],
  ["khairun@mail.com", 95, 85, 88],
  ["bondra@mail.com", 70, 75, 78],
  ["regi@mail.com", 91, 89, 93]
]

function viewScores(data, subject) {
  const arr = []
  for(let i = 1; i < data.length; i++){
    const objModel = {}
    let index;
    for(let j = 0; j < data[i].length; j++){
      if(data[0][j] === subject){
        index = j
      }
    }
    objModel.email = data[i][0]
    objModel.subject = data[0][index]
    objModel.points = data[i][index]
    arr.push(objModel)
  }
  console.log(arr)
}

// TEST CASE
viewScores(data, "quiz-1")
viewScores(data, "quiz-2")
viewScores(data, "quiz-3")

/*==========================================
  3. SOAL Recap Score (15 Poin + 5 Poin ES6)
  ==========================================
    Buatlah fungsi recapScore yang menampilkan perolehan nilai semua student. 
    Data yang ditampilkan adalah email user, nilai rata-rata, dan predikat kelulusan. 
    predikat kelulusan ditentukan dari aturan berikut:
    nilai > 70 "participant"
    nilai > 80 "graduate"
    nilai > 90 "honour"

    output:
    1. Email: abduh@mail.com
    Rata-rata: 85.7
    Predikat: graduate

    2. Email: khairun@mail.com
    Rata-rata: 89.3
    Predikat: graduate

    3. Email: bondra@mail.com
    Rata-rata: 74.3
    Predikat: participant

    4. Email: regi@mail.com
    Rata-rata: 91
    Predikat: honour

*/

function recapScores(data) {
  for(let i = 1; i < data.length; i++){
    const objModel = {}
    const arrPoints = []
    const arrSubject = []
    for(let j = 1; j < data[i].length; j++){
      arrPoints.push(data[i][j])
      arrSubject.push(data[i][j])
    }
    objModel.email = data[i][0]
    let formater = (x) => x % 1 === 0 ? x : x.toFixed(1) 
    objModel.average = formater(new Score(arrSubject, arrPoints, objModel.email).average())
    if(objModel.average > 70 && objModel.average < 80){
      objModel.predikat = 'participant'
    } else if(objModel.average > 80 && objModel.average < 90){
      objModel.predikat = 'graduate'
    } else if(objModel.average > 90){
      objModel.predikat = 'honour'
    } else {
      objModel.predikat = 'tidak lulus'
    }
    console.log(`${i}. Email: ${objModel.email}    
    Rata-rata: ${objModel.average}    
    Predikat: ${objModel.predikat}`)
    
    if(i < data.length){
      console.log('\n')
    }
  }
}

recapScores(data);

