//Soal No. 1
console.log("Soal No. 1")
function arrayToObject(arr){
  var result = "";
  var now = new Date();
  var thisYear = now.getFullYear();
  if(Array.isArray(arr)){
    for(var i = 0; i < arr.length; i++){
      var obj = {}
      obj.firtName = arr[i][0];
      obj.lastName = arr[i][1];
      obj.gender = arr[i][2];
      if(arr[i][3] < thisYear){
        obj.age = thisYear - arr[i][3];
      } else {
        obj.age = "Invalid birth year";
      }
      result = result + parseInt(i + 1)  +". " + obj.firtName + " " + obj.lastName 
      + " : { firstName: \""+ obj.firtName +"\", lastName: \"" + obj.lastName + "\", gender: \""+ obj.gender + "\", age: " + obj.age+ "}"
      if(i < arr.length - 1){
        result = result + "\n";
      }
    }
    return result;
  }
}
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ];
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
console.log(arrayToObject([["Abduh", "Muhamad", "male", 1992], ["Ahmad", "Taufik", "male", 1985]]));
console.log("\n");
console.log(arrayToObject(people));
console.log("\n");
console.log(arrayToObject(people2));

console.log("\n\nSoal No. 2");
function shoppingTime(memberId, money){
  var objResult = {};
  var listPurchased = [];
  var changeMoney = 0;
  var listItem = [
    {
      label: "Sepatu Stacattu",
      payment: 1500000
    },
    {
      label: "Baju Zoro",
      payment: 500000
    },
    {
      label: "Baju H&N",
      payment: 250000
    },
    {
      label: "Sweater Uniklooh",
      payment: 175000
    },
    {
      label: "Casing Handphone",
      payment: 50000
    }
  ]
  if(memberId !== undefined && memberId !== ''){
    if(money > 50000){
      changeMoney = money;
      for(var i = 0; i < listItem.length; i++){
        if(money > listItem[i].payment){
          listPurchased.push(listItem[i].label);
          changeMoney = changeMoney - listItem[i].payment
        }
      }
      objResult.memberId = memberId;
      objResult.money = money;
      objResult.listPurchased = listPurchased;
      objResult.changeMoney = changeMoney;
      return objResult;
    } else{
      return "Mohon maaf, uang tidak cukup"
    }
  } else{
    return "Mohon maaf, toko X hanya berlaku untuk member saja"
  }
}

console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log("\n");
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000));
console.log(shoppingTime('234JdhweRxa53', 15000));
console.log(shoppingTime());

//Soal No. 3
console.log("\n\nSoal No. 3");
function naikAngkot(listPenumpang){
  var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
  var srcIndex;
  var destIndex;
  var payment = 0;
  var arrResult = [];
  if(Array.isArray(listPenumpang) && listPenumpang.length !== 0){
    for(var i = 0; i < listPenumpang.length; i++){
      var objPenumpang = {};
      if(listPenumpang[i].length !== 0){
        for(var j = 0; j < rute.length; j++){
          if(listPenumpang[i][1] === rute[j]){
            srcIndex = j;
          }
          if(listPenumpang[i][2] === rute[j]){
            destIndex = j;
          }
          payment = Math.abs(srcIndex - destIndex) * 2000
        }
      }
      objPenumpang.nama = listPenumpang[i][0];
      objPenumpang.dari = listPenumpang[i][1];
      objPenumpang.tujuan = listPenumpang[i][2];
      objPenumpang.bayar = payment;
      arrResult.push(objPenumpang);
    }
    return arrResult;
  } else {
    return [];
  }
}
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));