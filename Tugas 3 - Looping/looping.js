//No. 1 Looping While
console.log("No. 1 Looping While");
console.log("LOOPING PERTAMA");
var i = 2;
while(i <= 20){
  console.log(i + " - I love coding");
  i += 2;
}

console.log("LOOPING KEDUA");
var j = 20;
while(j >= 2){
  console.log(j + " - I will become a mobile devoloper");
  j -= 2;
}


//No. 2 Looping menggunakan For
console.log("\n\nNo. 2 Looping menggunakan - For");
for(var i = 1; i <= 20; i++) {
  if(i % 2 === 0) {
    console.log(i + " - Berkualitas")
  } else {
    if(i % 3 === 0){
      console.log(i + " - I Love Coding");
    } else {
      console.log(i + " - Santai");
    }
  }
}

//No. 3 Membuat Persegi Panjang
console.log("\n\nNo. 3 Membuat Persegi Panjang")
for(var i = 0; i < 4; i++){
  var s = "";
  for(var j = 0; j < 8; j++){
    s = s.concat("#")
  }
  console.log(s);
}

//No. 4 Membuat Tangga
console.log("\n\nNo. 4 Membuat Tangga");
for(var i = 0; i < 7; i++){
  var s = "";
  for(var j = 0; j < i + 1; j++){
    s = s.concat("#")
  }
  console.log(s);
}

//No. 5 Membuat Papan Catur
console.log("\n\nNo. 5 Membuat Papan Catur");
for(var i = 0; i < 8; i++){
  var s = "";
  if(i % 2 === 0){
    for(var j = 0; j < 8; j++){
      if(j % 2 === 0){
        s = s.concat(" ");
      } else {
        s = s.concat("#");
      }
    }
  } else {
    for(var j = 0; j < 8; j++){
      if(j % 2 === 0){
        s = s.concat("#");
      } else {
        s = s.concat(" ");
      }
    }
  }
  console.log(s);
}   