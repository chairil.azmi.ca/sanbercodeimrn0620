var readBooks = require('./callback')

var books = [
  {
    name: 'LOTR',
    timeSpent: 3000
  },
  {
    name: 'Fidas',
    timeSpent: 2000
  },
  {
    name: 'Kalkulus',
    timeSpent: 4000
  }
]

function execute(waktu, index){
  readBooks(waktu, books[index], function(sisaWaktu){
    if(sisaWaktu !== 0){
      index = index + 1
      if(index < books.length){
        execute(sisaWaktu, index)
      } 
    }
  });
}

execute(10000, 0)