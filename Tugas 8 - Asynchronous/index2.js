var readBooksPromise = require('./promise')

var books = [
  {
    name: 'LOTR',
    timeSpent: 3000
  },
  {
    name: 'Fidas',
    timeSpent: 2000
  },
  {
    name: 'Kalkulus',
    timeSpent: 4000
  }
]

function execute(waktu, index){
  readBooksPromise(waktu, books[index]).then(res => {
    if(res !== 0){
      index = index + 1
      if(index < books.length){
        execute(res, index)
      } 
    }
  }).catch(err => {
    if(err){
      console.log(err)
    }
  })
}

execute(10000, 0)