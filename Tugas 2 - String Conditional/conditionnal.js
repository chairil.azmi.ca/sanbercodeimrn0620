//Tugas if-else
var nama = "John"
var peran = "Werewolf"

if(nama === ""){
  console.log("Nama Harus diisi!")
} else if(nama !== "" && peran === ""){
  console.log("Halo "+ nama + ", Pilih "+ peran + " untuk memulai game!")
} else {
  console.log("Selamat datang di Dunia Werewolf, " + nama)
  switch (peran) {
    case 'Penyihir':
      console.log("Halo " + peran + " " + nama + ", kamu dapat melihat siapa yang menjadi werewolf")
      break;
    case 'Guard':
      console.log("Halo " + peran + " " + nama + ", kamu akan membantu temanmu dari serangan werewolf")
      break;
    case 'Werewolf':
      console.log("Halo " + peran + " " + nama + ", kamu akan memakan mangsa tiap malam")
      break;
    default:
      console.log("Halo " + peran + " " + nama + ", peran tidak ada")
  }
}


//Tugas Switch Case
var tanggal = 1; 
var bulan = 12; 
var tahun = 1990; 

if(tanggal !== undefined && tanggal !== ""){
  if(tanggal < 1 || tanggal > 31){
    console.log('Tanggal Tidak Valid')
  } else {
    if (bulan !== undefined && bulan !== "") {
      switch(bulan){
        case 1:
          bulan = "Januari"
          break;
        case 2:
          bulan = "Februari"
          break;
        case 3:
          bulan = "Maret"
          break;
        case 4:
          bulan = "April"
          break;
        case 5:
          bulan = "Mei"
          break;
        case 6:
          bulan = "Juni"
          break;
        case 7:
          bulan = "Juli"
          break;
        case 8:
          bulan = "Agustus"
          break;
        case 9:
          bulan = "September"
          break;
        case 10:
          bulan = "Oktober"
          break;
        case 11:
          bulan = "November"
          break;
        case 12:
          bulan = "Desember"
          break;
        default:
          console.log('Bulan tidak Valid')
      }

      if(tahun !== undefined && tahun !== "" ){
        if(tahun < 1900 || tahun > 2200){
          console.log("Tahun Tidak Valid")
        } else {
          console.log(tanggal + " " + bulan + " " + tahun)
        }
      }
    } else {
      console.log("Bulan Harus Diisi")
    }
  }  
} else {
  console.log("Tanggal Harus Diisi")
}